<html>
<head>
    <?php
    session_start();
    $GLOBALS["maxVersuche"] = 10; //maximal 10 Versuche
    $GLOBALS["buchstaben"] = "ABCDEFG"; //Buchstaben die möglich sind
    $GLOBALS["länge"] = 4; //maximale Eingabe in das Textfeld
    class Zeile {
        var $kreise;
        var $buchstaben;
        var $groesse;

        function __construct($groesse) {
            $this->groesse = $groesse;
            $kreise = array();
            $buchstaben = "";
        }

        function zeilezeichnen() {
            echo '<div>';
            echo '';
            strtoupper($this->buchstaben);
            for($i=0;$i<strlen($this->buchstaben);$i++) {				//Buchstaben Ausgabe
                $bt1 = new Buchstaben($this->buchstaben{$i});
                $bt1->buchstabezeichnen();
            }
            for(;$i<$this->groesse;$i++) {								//Mit Leerzeichen füllen, falls Platz
                $bt1 = new Buchstaben(' ');
                $bt1->buchstabezeichnen();
            }
            echo '</div>';
            echo '<div>';
            $i=0;
            if(is_array($this->kreise)) {							//Kreise ausgeben
                foreach($this->kreise as $Kreis) {
                    $Kreis->kreiszeichnen();
                    $i++;
                }
            }
            $Kreis = new Kreis("weiss");
            for(;$i<$this->groesse;$i++)
                $Kreis->kreiszeichnen();
            echo '</div>';

            echo '</div>';
        }
    }
    function vergleich($versucht,$buchstaben,$lange,$zeile) {		//Überprüft ob Lösung stimmt
        $zeile->buchstaben = strtoupper($versucht); //in Großbuchstaben umwandeln
        $loesungversuch = str_split(strtoupper($versucht));
        $loesung = $_SESSION['loesung'];
        $tried = Array();
        $Position_stimmt = 0;
        $enthalten = 0;
        for($i=0;$i<$lange;$i++) {
            if(in_array($loesungversuch[$i],$loesung)) { //vergleicht beide Arrays
                if($loesungversuch[$i] === $loesung[$i]) //vergleicht ob 1 Buchstabe == 1 Buchstabe von Lösung
                    $Position_stimmt++;
                else if(in_array($loesungversuch[$i],$tried)=== false) //vergleicht ob Buchstabe == in Lösung enthalten
                    $enthalten++;
            }
            $tried[$i] = $loesungversuch[$i];
        }
        for($i=0;$i<$lange;$i++) {
            if($i<$Position_stimmt)
                $zeile->kreise[$i] = new Kreis("rot");
            else if($i<$Position_stimmt+$enthalten)
                $zeile->kreise[$i] = new Kreis("schwarz");
            else
                $zeile->kreise[$i] = new Kreis("weiss");
        }
        return $Position_stimmt == $lange;
    }

    function vergleich_loesung($versucht,$buchstaben,$lange) {		//Überprüft, ob der Code gültig ist
        if(strlen($versucht) != $lange)
            return false;
        $arrCode = str_split(strtoupper($versucht));
        $arrLetters = str_split($buchstaben);
        $counter = 0;
        foreach($arrCode as $key)
            if(in_array($key,$arrLetters))
                $counter++;

        return $counter == $lange;
    }

    function resetGame($maxVersuche,$lange,$buchstaben) {		//Neustart
        $_SESSION['versuche'] = 0;
        $_SESSION['loesung'] = "";
        $_SESSION['zeilen'] = array();
        unset($_SESSION['finished']);
        $lettersCopy = $buchstaben;
        for($i=0;$i<$maxVersuche;$i++)
            $_SESSION['zeilen'][$i] = new Zeile($lange);
        for($i=0;$i<$lange;$i++) {
            $input = array("A", "B", "C", "D", "E", "F", "G", "H");
            $rand_keys = array_rand($input, 4);
            $randomNum = rand(0,strlen($lettersCopy)-1);
            $_SESSION['loesung'][$i] = $lettersCopy[$randomNum];
            $arr = str_split($lettersCopy); //String zu array
            unset($arr[$randomNum]);
            $lettersCopy = implode('', $arr); //array-Element zu String verbinden
        }

    }
    class Kreis {
        var $kreisfarbe;

        function __construct($kreisfarbe) {
            $this->kreisfarbe = $kreisfarbe;
        }

        function kreiszeichnen() {
            echo '<img src="Bilder/'.$this->kreisfarbe.'.png" style="height: 25px; width: 25px;margin-left: 5px; margin-right: 5px;">';
        }
    }

    class Buchstaben {
        var $bt1;

        function __construct($bt1) {
            $this->bt1 = $bt1;
        }

        function buchstabezeichnen() {
            echo '<div style="line-height: 25px;margin-left:5px; margin-right: 5px; float: left; ">'.$this->bt1.'</div>';
        }
    }


    ?>
</head>
<body>
<fieldset><legend>Codebreaker</legend>
    <?php
    $maxVersuche = $GLOBALS["maxVersuche"];
    $lange = $GLOBALS["länge"];
    $buchstaben = $GLOBALS["buchstaben"];

    if(!isset($_SESSION['loesung']) || !isset($_SESSION['versuche']) || !isset($_SESSION['zeilen']) || isset($_GET['reset']))	//überprüft ob Variablen gesetzt
        resetGame($maxVersuche,$lange,$buchstaben);

    $zeile = $_SESSION['zeilen'];
    $versuche = $_SESSION['versuche'];

    if(isset($_POST['deinversuch']) && isset($_POST['loesung'])) {		//überprüft ob Versuch und Code gesetzt sind
        if(vergleich_loesung($_POST['loesung'],$buchstaben,$lange)) {
            if(vergleich($_POST['loesung'],$buchstaben,$lange,$zeile[$versuche])){
                $versuche += 1;
                echo("<br /><b>Ouh man! Bereits beim ".$versuche." Versuch alles richtig. Starke Leistung!</b><br /><br /><br />");
            }
            else {
                $versuche++;


            }
        }

    }
    $_SESSION['versuche'] = $versuche;
    $_SESSION['zeilen'] = $zeile;
    $maxVersuche = $GLOBALS["maxVersuche"];
    $lange = $GLOBALS["länge"];
    $buchstaben = $GLOBALS["buchstaben"];
    echo "Bitte wähle 4 von den folgenden Buchstaben aus: ".$buchstaben."<br />";
    echo "Du hast <b><u>" .$versuche. "</u></b> von 10 Versuche verbraucht!";

    for($i=$maxVersuche-1;$i>=0;$i--)
        $_SESSION['zeilen'][$i]->zeilezeichnen();
    ?>
    <div >

        <form action="/Codebreaker/index.php" method="post">
            <input type="textfield" style="width: 100px;" maxlength="4" name="loesung" >
            <input type="submit" name="deinversuch" value="Versuchen!">
            <a href="/Codebreaker/index.php?reset"><input type="button" name="reset" value="Neues Spiel!"></a>
        </form>
</fieldset>
</div>
</body>
</html>